//
//  ResultsViewController.swift
//  face
//
//  Created by Admin on 13.01.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

import UIKit
import Social
import GoogleMobileAds
protocol ResultsViewControllerDelegate{
    func resultsClosed(controller: ResultsViewController)
}

class ResultsViewController: GAITrackedViewController, UIDocumentInteractionControllerDelegate, VKSdkDelegate{
    
    var delegate: ResultsViewControllerDelegate? = nil
    let instagram = FInstagram()
    let locale = NSLocale.currentLocale()
  
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var bgView: UIImageView!
    
    @IBOutlet weak var fbShareButton: UIButton!
    var texts:NSDictionary?
    
    func getStrings() -> NSDictionary {
        var dict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Strings", ofType: "plist"){
            dict = NSDictionary(contentsOfFile: path)
        } else {
            dict = NSDictionary()
        }
        
        return dict!
    }
    
    override func viewDidAppear(animated: Bool) {
        self.screenName = "Results screen"
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        texts = getStrings()
        let options = texts?["options"] as NSArray

        var fbImage = UIImage(named: "facebook")
        
        println(NSLocale.currentLocale().localeIdentifier)
        
        if (NSLocale.currentLocale().localeIdentifier == "ru"){
            fbImage = UIImage(named: "vkontakte")
        }
        
        fbShareButton.setImage(fbImage, forState: .Normal)
        
        bannerView.adUnitID = Settings.getSettings()["AdMob-banner"] as String
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [ "8e3a5fa93999cd2bdc397c5506877d63", "acb3c2a9b255e473cd1390ebd19eb9afcf0b16b2" ]
        bannerView.loadRequest(request)
    
//        bgView.image = UIImage(named: options[randomIndex] + "_blur")
        var checkImage = mainImageView.image as UIImage!
        var catFileName = ""
        var catName = ""
        while (checkImage == nil){
            var randomIndex = Int(arc4random_uniform(UInt32(options.count)))
           
            catFileName = String(format: "cat%02d", randomIndex+1)
            catName = options[randomIndex] as String
            println(catFileName)
            checkImage = UIImage (named: catFileName)
           
        }
        //var originalWidth  = checkImage.size.width
        
        mainImageView.image=textToImage(catName, inImage: checkImage!, atPoint: CGPointMake(50, 35))
        // Do any additional setup after loading the view.
    }

    func textToImage(drawText: NSString, inImage: UIImage, atPoint:CGPoint)->UIImage{
        
        // Setup the font specific variables
        var textColor: UIColor = UIColor.whiteColor()
        var textFont: UIFont = UIFont(name: "Helvetica Bold", size: 30)!
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(inImage.size)
        
        //Setups up the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
        ]
        
        //Put the image into a rectangle as large as the original image.
        inImage.drawInRect(CGRectMake(20, 20, 460, 460))
        
        let color : UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        color.setFill()
        UIRectFillUsingBlendMode(CGRectMake(20, 20, 460, 70), kCGBlendModeDarken)
        
        
        var frameImage: UIImage! = UIImage(named: "frame")
        frameImage.drawInRect(CGRectMake(0, 0, 500, 500))
        
        
        
        // Creating a point within the space that is as bit as the image.
        var rect: CGRect = CGRectMake(atPoint.x, atPoint.y, 400, 400)
        
        //Now Draw the text into an image.
        drawText.drawInRect(rect, withAttributes: textFontAttributes)
        
        // Create a new image out of the images we have created
        var newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //And pass it back up to the caller.
        return newImage
        
    }
    

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        bgView.image = nil
        mainImageView.image = nil
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func repeatScan(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_repeat", value: nil).build())
    
    }
    
    @IBAction func shareTwitter(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_share_tw", value: nil).build())
        
        if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)){
            var controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter);
            controller.addImage(mainImageView.image!)
            controller.setInitialText((texts?["sharing"] as NSDictionary)["after_scan"] as String)
            
            self.presentViewController(controller, animated: true, completion: nil)
        }
        
    }
    @IBAction func shareInstagram(sender: AnyObject) {
        let caption = (texts?["sharing"] as NSDictionary)["after_scan"] as NSString
        
        instagram.postImage(mainImageView.image!, caption: caption , inView: self.view, delegate: self)
    }
    
    @IBAction func shareFacebook(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        if (NSLocale.currentLocale().localeIdentifier == "ru"){
            
            tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_share_vk", value: nil).build())
            
            VKSdk.initializeWithDelegate(self, andAppId: Settings.getSettings()["VK-app-id"] as String)
            var controller = VKShareDialogController()
            controller.text = (texts?["sharing"] as NSDictionary)["after_scan"] as String
            var image = VKUploadImage(image: mainImageView.image, andParams: nil)
            controller.uploadImages = [image]
            controller.completionHandler = { result in
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            self.presentViewController(controller, animated: true, completion: nil)

            
        } else {
            
            tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_share_fb", value: nil).build())
            
            
            if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)){
                var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook);
                controller.addImage(mainImageView.image!)
                controller.setInitialText((texts?["sharing"] as NSDictionary)["after_scan"] as String)
                
                self.presentViewController(controller, animated: true, completion: nil)
            }
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "startSegue"){
            let vc = segue.destinationViewController as StartViewController
            vc.showAds = true
        }
    }
    
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
    }
    
    func vkSdkTokenHasExpired(expiredToken: VKAccessToken!) {
        
    }
    
    func vkSdkUserDeniedAccess(authorizationError: VKError!) {
    }
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
    }
    
    func vkSdkReceivedNewToken(newToken: VKAccessToken!) {
    }
    
    func vkSdkIsBasicAuthorization() -> Bool {
        return false
    }
        
    
    
    
    
}
