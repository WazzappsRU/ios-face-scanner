//
//  ViewController.swift
//  face
//
//  Created by Admin on 06.12.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

import UIKit

var videoFilter: CoreImageVideoFilter?
var detector: CIDetector?
import GoogleMobileAds
class ViewController: GAITrackedViewController, GADInterstitialDelegate {

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var border = UIView()
    var lastTime = CACurrentMediaTime()

    var interstitial: GADInterstitial?
    var adsLoaded = false
    @IBOutlet weak var helpLabel: UILabel!
    var videoFilter: CoreImageVideoFilter?
    
    
    var texts:NSDictionary!
    

    
    
    override func viewDidAppear(animated: Bool) {
        self.screenName = "Scan screen"
        super.viewDidAppear(animated)
        
    }
    
    func getStrings() -> NSDictionary {
        var dict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Strings", ofType: "plist"){
            dict = NSDictionary(contentsOfFile: path)
        } else {
            dict = NSDictionary()
        }
        
        return dict!
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
                // Do any additional setup after loading the view, typically from a nib.
        videoFilter = CoreImageVideoFilter(superview: videoView, applyFilterCallback: {
            image in
            return self.performFaceDetection(image)
            }
        )
        
        self.helpLabel.hidden = false
        self.scanButton.hidden = true
        
        detector = prepareFaceDetector()
        videoFilter?.startFiltering()
        border.layer.cornerRadius = 10;
        border.layer.borderWidth = 1
        border.layer.borderColor = UIColor.whiteColor().CGColor
        border.hidden = true
        self.view.addSubview(border)
        
        interstitial = GADInterstitial()
        interstitial?.adUnitID = Settings.getSettings()["AdMob-interstitial-camera"] as String
        interstitial?.delegate = self
        let request = GADRequest()
        request.testDevices = [ "8e3a5fa93999cd2bdc397c5506877d63", "acb3c2a9b255e473cd1390ebd19eb9afcf0b16b2" ]
        
        bannerView.adUnitID = Settings.getSettings()["AdMob-banner"] as String
        bannerView.rootViewController = self
        let bannerRequest = GADRequest()
        bannerRequest.testDevices = [ "8e3a5fa93999cd2bdc397c5506877d63", "acb3c2a9b255e473cd1390ebd19eb9afcf0b16b2" ]
        bannerView.loadRequest(bannerRequest)
        
        interstitial?.loadRequest(request)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func performFaceDetection(image: CIImage) -> CIImage? {
        let currentTime = CACurrentMediaTime()
        if (currentTime - lastTime > 0.3){
            var detected = false
            if let detector = detector {
            // Get the detections
            let features = detector.featuresInImage(image)
            for feature in features as [CIFaceFeature] {
                detected = true
                let face = CGRect(x: feature.leftEyePosition.x - 100, y: feature.leftEyePosition.y-150, width: CGFloat(200 ), height: CGFloat(200))
                let transform = CGAffineTransformMakeScale(1, -1);
                    let transformToUIKit = CGAffineTransformTranslate(transform, image.extent().width,0);
                    let translatedRect = CGRectApplyAffineTransform(face, transformToUIKit);
                    let myRect = CGRect(
                        x: translatedRect.minX * (self.view.bounds.width / image.extent().width),
                        y: translatedRect.minY * (self.view.bounds.height / image.extent().height),
                        width: 200,
                        height: 200
                        )
                    dispatch_async(dispatch_get_main_queue(), {
                        self.border.frame = myRect
                    })
                break;
            }
        
        if (self.scanButton.hidden == detected){
            dispatch_async(dispatch_get_main_queue(), {
                self.helpLabel.hidden = self.scanButton.hidden
                self.scanButton.hidden = !self.scanButton.hidden
                self.border.hidden = self.scanButton.hidden
            });
        }
        }
        lastTime = currentTime
        }
        return image
    }
    
    func prepareFaceDetector() -> CIDetector {
        let options = [CIDetectorAccuracy: CIDetectorAccuracyLow]
        return CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options)
    }
    
    @IBAction func changeCamera(sender: AnyObject) {
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "scan_screen_change_camera", value: nil).build())

        videoFilter?.changeDevice()
        if (adsLoaded){
            interstitial?.presentFromRootViewController(self)
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "analyzeSegue" {
            let vc = segue.destinationViewController as AnalyzeViewController
            
            vc.image = videoFilter?.resultImage

        }
        
        

    }
    
    func interstitialDidReceiveAd(ad: GADInterstitial!) {
        adsLoaded = true
    }
    
    @IBAction func backHome(sender: AnyObject) {
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "scan_screen_home", value: nil).build())

    }
    @IBAction func gotoResults(sender: AnyObject) {
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "scan_screen_scan", value: nil).build())

        videoFilter?.stopFiltering()
    }
}

