//
//  FInstagram.swift
//  face
//
//  Created by Admin on 11.02.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

import Foundation

class FInstagram {
    
    var documentIteractionController: UIDocumentInteractionController?
    
    init(){
        
    }
    
    func isInstalled() -> Bool {
        
        let url = NSURL(string: "instagram://app")
        return UIApplication.sharedApplication().canOpenURL(url!)
        
    }
    
    func postImage(image:UIImage, caption: NSString, inView: UIView, delegate: ResultsViewController){
        let path = NSHomeDirectory().stringByAppendingPathComponent("Documents/tempinstgramphoto.ig")
        UIImageJPEGRepresentation(image, 1.0).writeToFile(path, atomically: true)
        
        let fileUrl = NSURL(fileURLWithPath: path)
        documentIteractionController = UIDocumentInteractionController(URL: fileUrl!)
        documentIteractionController!.UTI = "com.instagram.photo"
        documentIteractionController!.delegate = delegate
        let annotation:NSDictionary = [
            "InstagramCaption" : caption
        ]
        documentIteractionController!.annotation = annotation
        documentIteractionController!.presentOpenInMenuFromRect(CGRectZero, inView: inView, animated: true)
        
        
    }
    
    
}