//
//  StartViewController.swift
//  face
//
//  Created by Admin on 25.01.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

import UIKit
import Social
import GoogleMobileAds

class StartViewController: GAITrackedViewController, GADInterstitialDelegate, VKSdkDelegate {
    
    var interstitial: GADInterstitial?

    var showAds = false
    @IBOutlet weak var bannerView: GADBannerView!
    
    var texts:NSDictionary!
    
    override func viewDidAppear(animated: Bool) {
        self.screenName = "Start screen"
        super.viewDidAppear(animated)
        
    }
    
    func getStrings() -> NSDictionary {
        var dict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Strings", ofType: "plist"){
            dict = NSDictionary(contentsOfFile: path)
        } else {
            dict = NSDictionary()
        }
        
        return dict!
    }

    @IBAction func shareTW(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "start_screen_share_tw", value: nil).build())
        
        if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)){
            var controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter);
            
            controller.setInitialText((texts["sharing"] as NSDictionary)["pre_scan"] as NSString)
            
            self.presentViewController(controller, animated: true, completion: nil)
        }

        
    }
    @IBAction func shareVK(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "start_screen_share_vk", value: nil).build())
        
        VKSdk.initializeWithDelegate(self, andAppId: Settings.getSettings()["VK-app-id"] as String)
        var controller = VKShareDialogController()
        controller.text = (texts["sharing"] as NSDictionary)["pre_scan"] as NSString
        controller.completionHandler = { result in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        self.presentViewController(controller, animated: true, completion: nil)

        
    }
    @IBAction func shareFB(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "start_screen_share_fb", value: nil).build())
        if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)){
            var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook);            controller.setInitialText((texts["sharing"] as NSDictionary)["pre_scan"] as NSString)
            
            self.presentViewController(controller, animated: true, completion: nil)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        texts = getStrings()
        interstitial = GADInterstitial()
        interstitial?.adUnitID = Settings.getSettings()["AdMob-interstitial-start"] as String
        interstitial?.delegate = self
        let request = GADRequest()
        request.testDevices = [ "8e3a5fa93999cd2bdc397c5506877d63", "acb3c2a9b255e473cd1390ebd19eb9afcf0b16b2" ]
        
        interstitial?.loadRequest(request)
        
        bannerView.adUnitID = Settings.getSettings()["AdMob-banner"] as String
        bannerView.rootViewController = self
        let bannerRequest = GADRequest()
        bannerRequest.testDevices = [ "8e3a5fa93999cd2bdc397c5506877d63", "acb3c2a9b255e473cd1390ebd19eb9afcf0b16b2" ]
        bannerView.loadRequest(bannerRequest)
        
        gaSignin()
        // Do any additional setup after loading the view.
    }
    
    func gaSignin(){
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.set("&uid", value: "123")
        
        tracker.set(GAIFields.customDimensionForIndex(1), value: "123")
        
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("UX", action: "user signed in", label: nil, value: nil).build())

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func interstitialDidReceiveAd(ad: GADInterstitial!) {
        if (showAds){
            interstitial?.presentFromRootViewController(self)
        }
    }
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
    }
    
    func vkSdkTokenHasExpired(expiredToken: VKAccessToken!) {
        
    }
    
    func vkSdkUserDeniedAccess(authorizationError: VKError!) {
    }
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
    }
    
    func vkSdkReceivedNewToken(newToken: VKAccessToken!) {
    }
    
    func vkSdkIsBasicAuthorization() -> Bool {
        return false
    }

}
